[![GoDoc](https://godoc.org/goba.gopherpit.com/bracketed?status.svg)](https://godoc.org/goba.gopherpit.com/bracketed)

# Purpose

Package bracketed provides an io.Writer where each opaque write, excluding an
optional newline at the end, is bracketed by a pair of user-specifiable strings.
For example, writing " abc \n" with brackets "<" and ">" yields "< abc >\n".

A byte is transparent if it is either space or newline. If a write contains a
non-transparent byte, then the write is opaque.

# Usage

```go
import "goba.gopherpit.com/bracketed"

var bw io.Writer = bracketed.NewWriter(w, "<", ">")
```
