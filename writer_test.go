package bracketed_test

import (
	"bytes"
	"io"
	"testing"

	"goba.gopherpit.com/bracketed"
)

func TestWrite(t *testing.T) {
	expected := []struct {
		in   string
		outb int // 1 iff bracket
		outs string
	}{
		// space
		{"   ", 0, "   "},
		// newline
		{"\n", 0, "\n"},
		{"\r", 0, "\r"},
		{"\r\n", 0, "\r\n"},
		// ending with newline
		{" \n", 0, " \n"},
		{" abc \n", 1, "< abc >\n"},
		// others
		{" abc", 1, "< abc>"},
		{"\t", 1, "<\t>"},
		{"abc ", 1, "<abc >"},
	}
	for _, row := range expected {
		// Make a new Writer.
		buf := &bytes.Buffer{}
		var bw io.Writer = bracketed.NewWriter(buf, "<", ">")
		// Write and retrieve result.
		n, err := bw.Write([]byte(row.in))
		if err != nil {
			t.Fatalf("ER: %v", err)
		}
		s := buf.String()
		// Test.
		if m := len(row.in) + 2*row.outb; m != n || row.outs != s {
			t.Fatalf("XX: Input %#v; expect (%d, %#v); got (%d, %#v).",
				row.in, m, row.outs, n, s)
		}
		if testing.Verbose() {
			t.Logf("OK: %#-10v -> (%d, %#v)", row.in, n, s)
		}
	}
}
