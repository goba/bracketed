/*

Package bracketed provides an io.Writer where each opaque write, excluding an
optional newline at the end, is bracketed by a pair of user-specifiable strings.
For example, writing " abc \n" with brackets "<" and ">" yields "< abc >\n".

A byte is transparent if it is either space or newline. If a write contains a
non-transparent byte, then the write is opaque.

*/
package bracketed // import "goba.gopherpit.com/bracketed"

import (
	"bytes"
	"io"
)

// Writer is the concrete type for export.
type Writer struct {
	w          io.Writer
	begin, end string
}

// NewWriter constructs a new bracketed.Writer.
func NewWriter(w io.Writer, begin, end string) *Writer {
	return &Writer{w, begin, end}
}

// Write makes w an io.Writer. The number of bytes written n also counts the
// bytes in the brackets.
func (w *Writer) Write(p []byte) (n int, err error) {
	l := len(p)
	// Case 0: nothing to write.
	if l == 0 {
		return
	}
	// Detect nl: \r, \r\n, \n.
	{
		sn0 := p[l-1] == '\n'
		sr0 := p[l-1] == '\r'
		sr1 := sn0 && l >= 2 && p[l-2] == '\r'
		if sr1 { // \r\n
			l -= 2
		} else if sn0 || sr0 { // only \n or \r
			l--
		}
	}
	// Case 1: nothing left to write (but can still have nl).
	if l == 0 {
		return w.w.Write(p)
	}
	// Case 2: bracket and write.
	var m int
	justSpaces := len(bytes.TrimLeft(p[:l], " ")) == 0
	// begin
	if !justSpaces {
		m, err = w.w.Write([]byte(w.begin))
		n += m
		if err != nil {
			return
		}
	}
	// zs
	m, err = w.w.Write(p[:l])
	n += m
	if err != nil {
		return
	}
	// end
	if !justSpaces {
		m, err = w.w.Write([]byte(w.end))
		n += m
		if err != nil {
			return
		}
	}
	// nl
	m, err = w.w.Write(p[l:])
	n += m
	return
}
